<!DOCTYPE html>
<html lang="es">
    @include('_statics._header')
    <body>
        <div class="mdl-grid">

            @yield('content')
            
            @include('_statics._globalJS')

        </div><!-- End .container -->
    </body>
</html>

    
