<h3>Hola {{ $nombre }}</h3>

<div>
    <p>Este es un mail automático generado para informar que he recibido el mensaje correctamente</p>
    <p>En cuanto lo haya leido me pondré en contacto con usted en el mail que ha proporcionado: {{ $email }}</p>
</div>

<hr>
