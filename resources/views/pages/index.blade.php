@extends('main')

@php($d=[])
@foreach ($datos as $i => $dato)
    @php
    $d[$dato->nombre] = $dato->dato
    @endphp
@endforeach

@section('fixedBlock')
<!-- FAB button with ripple -->
<button id="main-menu-button" class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect">
  <i class="material-icons">menu</i>
</button>
<ul class="mdl-menu mdl-menu--bottom-left mdl-js-menu mdl-js-ripple-effect"
      for="main-menu-button">
  <li class="mdl-menu__item"><a href="#inicioBlockBlock" class="mdl-navigation__link mdl-button--colored">Datos</a></li>
  <li class="mdl-menu__item"><a href="#sobreMiBlock" class="mdl-navigation__link">Sobre Mi</a></li>
  <li class="mdl-menu__item"><a href="#experienciaBlock" class="mdl-navigation__link">Experiencia</a></li>
  <li class="mdl-menu__item"><a href="#habilidadesBlock" class="mdl-navigation__link">Habilidades</a></li>
  <li class="mdl-menu__item"><a href="#contactoBlock" class="mdl-navigation__link">Contactar</a></li>
</ul>
<div class="center avatar">
  <img src="{{ asset('imgs/'.$d['Imagen']) }}">
</div>
@endsection

@section('oneBlock')
<div class="mdl-grid">
  <div class="mdl-cell mdl-cell--12-col">
    <div class="mdl-cell--8-col mdl-cell--2-offset">
      <div class="title-name">
        {{ $d['Nombre'] . ' ' . $d['Apellido1'] . ' ' . $d['Apellido2'] }}
      </div>
    </div>
    <div class="mdl-cell--6-col mdl-cell--5-offset">
      <div class="sub-title-name">
        {{ $d['Ocupacion'] }}
      </div>
    </div>
    <div class="mdl-cell--4-col mdl-cell--4-offset">
      <div class="social-content">
          <ul>
              @foreach ($rrss as $i => $rs)
                  <li class="social"><a href="{{ $rs->url }}" class="{{ $rs->rrss }}"> </a></li>
              @endforeach
          </ul>
      </div>
    </div>
  </div>
</div>
@endsection

@section('twoBlock')
<div class="mdl-grid">
  <div class="mdl-cell mdl-cell--12-col top-spacing-30">
    <div class="title-about">
      Sobre Mi
    </div>
  </div>
  <div class="mdl-cell mdl-cell--3-col top-spacing-100">
    <table class="mdl-data-table mdl-js-data-table mdl-shadow--0dp mdl-cell mdl-cell--12-col">
      <tbody>
        <tr>
          <td class="chip white">
            Nacimiento
          </td>
          <td>
            {{ Date::parse($d['Nacimiento'])->format('d \d\e F \d\e Y') }}
          </td>
        </tr>
        <tr>
          <td class="chip white">
            Fijo
          </td>
          <td>
            {{ $d['Fijo'] }}
          </td>
        </tr>
        <tr>
          <td class="chip white">
            Movil
          </td>
          <td>
            {{ $d['Movil'] }}
          </td>
        </tr>
        <tr>
          <td class="chip white">
            e-Mail
          </td>
          <td>
            {{ $d['Email'] }}
          </td>
        </tr>
        <tr>
          <td class="chip white">
            Site
          </td>
          <td>
            {{ $d['Website'] }}
          </td>
        </tr>
        <tr>
          <td class="chip white">
            Dirección
          </td>
          <td>
            {{ $d['Calle'] . ' ' . $d['Numero'] }}
          </td>
        </tr>
        <tr>
          <td class="chip white">
            Código  Postal
          </td>
          <td>
            {{ $d['cPostal'] }}
          </td>
        </tr>
        <tr>
          <td class="chip white">
            Nacionalidad
          </td>
          <td>
            {{ $d['Pais'] }}
          </td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="about-extra-icon mdl-cell mdl-cell--8-col mdl-cell--1-offset top-spacing-100">
    <ul class="hobbies">
      @foreach($hobs as $i => $hob)
      <li>
        <p>
          <i class="fa fa-{{$hob->fa_icon}} fa-lg"></i>
          <br>
          <span>
            {{ $hob->nombre }}
          </span>
        </p>
      </li>
      @endforeach
    </ul>
  </div>
</div>
@endsection

@section('threeBlock')
<div class="mdl-grid">
  <div class="mdl-cell mdl-cell--12-col top-spacing-30">
    <div class="title-about">
      Experiencia
    </div>
  </div>
  <div class="mdl-cell mdl-cell--5-col mdl-cell--1-offset top-spacing-50">
    <div class="container">
      <ul class="timeline bottom-spacing-50">
        <!-- foreach -->
        @foreach($jobs as $i => $job)
          <li class="item-timeline"><span></span>
            <div>
              <div class="title">{{ $job->empresa }}</div>
              <div class="info">{{ $job->titulo }}</div>
              <div class="type"><button type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect modal__trigger mdl-color-text--teal" data-modal="#modal{{$job->id}}">+ Info</button></div>
            </div> <span class="number right"><span>{{ Date::parse($job->fecha_fin)->format('m/Y') == "01/0001" ? "Actual" : Date::parse($job->fecha_fin)->format('m/Y') }}</span> <span>{{ Date::parse($job->fecha_inicio)->format('m/Y') }}</span></span>
          </li>
          <div id="modal{{$job->id}}" class="modal modal__bg">
            <div class="modal__dialog">
              <div class="modal__content">
                <div class="modal__header">
                  <div class="modal__title">
                    <h2 class="modal__title-text">{{ $job->empresa }}</h2>
                  </div>
                </div>
                <div class="modal__text mdl-cell mdl-cell--12-col">
                  <h6>{{ $job->titulo }}</h6>
                  {!! $job->descripcion !!}
                </div>
                <div class="modal__footer">
                  <button type="button" class="mdl-button mdl-js-button mdl-button--raised modal__close">Cerrar</button>
                </div>
              </div>
            </div>
          </div>
        @endforeach
      </ul>
    </div>
  </div>
  <div class="mdl-cell mdl-cell--5-col mdl-cell--1-offset top-spacing-50">
    <div class="container">
      <ul class="timeline-left bottom-spacing-50">
        <!-- foreach -->
        @foreach($grades as $i => $grade)
          <li class="item-timeline"><span></span>
            <div>
              <div class="title">{{ $grade->colegio }}</div>
              <div class="info">{{ $grade->titulo }}</div>
              <div class="type"><button type="button" class="mdl-button mdl-js-button mdl-js-ripple-effect modal__trigger mdl-color-text--teal" data-modal="#modal{{$grade->id}}">+ Info</a></div>
            </div> <span class="number"><span>{{ Date::parse($grade->fecha_fin)->format('m/Y') == "01/0001" ? "Actual" : Date::parse($grade->fecha_fin)->format('m/Y') }}</span> <span>{{ Date::parse($grade->fecha_inicio)->format('m/Y') }}</span></span>
          </li>
          <div id="modal{{$grade->id}}" class="modal modal__bg">
            <div class="modal__dialog">
              <div class="modal__content">
                <div class="modal__header">
                  <div class="modal__title">
                    <h2 class="modal__title-text">{{ $grade->colegio }}</h2>
                  </div>
                </div>
                <div class="modal__text mdl-cell mdl-cell--12-col">
                  <h6>{{ $grade->titulo }}</h6>
                  {!! $grade->descripcion !!}
                </div>
                <div class="modal__footer">
                  <button type="button" class="mdl-button mdl-js-button mdl-button--raised modal__close">Cerrar</button>
                </div>
              </div>
            </div>
          </div>
        @endforeach
      </ul>
    </div>
  </div>
</div>
@endsection

@section('fourBlock')
<div class="mdl-grid">
  <div class="mdl-cell mdl-cell--12-col top-spacing-30">
    <div class="title-about">
      Habilidades
    </div>
  </div>
  <div class="mdl-cell mdl-cell--8-col mdl-cell--2-offset top-spacing-150">
    <div class="mdl-color--white mdl-shadow--0dp mdl-cell mdl-cell--12-col mdl-grid">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" width="200" height="200">
            <linearGradient id="lg1" x1="0.5" y1="1" x2="0.5" y2="0">
                <stop offset="0%" stop-opacity="1" stop-color="darkorange"/>
                <stop offset="30%" stop-opacity="1" stop-color="darkorange"/>
                <stop offset="30%" stop-opacity="0" stop-color="darkorange"/>
                <stop offset="100%" stop-opacity="0" stop-color="darkorange"/>
            </linearGradient>
            <circle cx="50" cy="50" r="45" fill="url(#lg1)" stroke="grey" stroke-width="5"/>
            <text x="53" y="45" font-family="Roboto" font-size="1em" fill="#000" text-anchor="middle" dy="45">30%</text>
            <text x="50" y="0" font-family="Roboto" font-size="1em" fill="#000" text-anchor="middle" dy="45">PHP</text>
          </svg>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" width="200" height="200">
            <linearGradient id="lg2" x1="0.5" y1="1" x2="0.5" y2="0">
                <stop offset="0%" stop-opacity="1" stop-color="royalblue"/>
                <stop offset="50%" stop-opacity="1" stop-color="royalblue"/>
                <stop offset="50%" stop-opacity="0" stop-color="royalblue"/>
                <stop offset="100%" stop-opacity="0" stop-color="royalblue"/>
            </linearGradient>
            <circle cx="50" cy="50" r="45" fill="url(#lg2)" stroke="grey" stroke-width="5"/>
            <text x="53" y="45" font-family="Roboto" font-size="1em" fill="#000" text-anchor="middle" dy="45">50%</text>
            <text x="50" y="0" font-family="Roboto" font-size="1em" fill="#000" text-anchor="middle" dy="45">Javascript</text>
          </svg>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" width="200" height="200">
            <linearGradient id="lg3" x1="0.5" y1="1" x2="0.5" y2="0">
                <stop offset="0%" stop-opacity="1" stop-color="forestgreen"/>
                <stop offset="80%" stop-opacity="1" stop-color="forestgreen"/>
                <stop offset="80%" stop-opacity="0" stop-color="forestgreen"/>
                <stop offset="100%" stop-opacity="0" stop-color="forestgreen"/>
            </linearGradient>
            <circle cx="50" cy="50" r="45" fill="url(#lg3)" stroke="grey" stroke-width="5"/>
            <text x="53" y="45" font-family="Roboto" font-size="1em" fill="#000" text-anchor="middle" dy="45">80%</text>
            <text x="50" y="0" font-family="Roboto" font-size="1em" fill="#000" text-anchor="middle" dy="45">jQuery</text>
          </svg>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" width="200" height="200">
            <linearGradient id="lg1" x1="0.5" y1="1" x2="0.5" y2="0">
                <stop offset="0%" stop-opacity="1" stop-color="darkorange"/>
                <stop offset="30%" stop-opacity="1" stop-color="darkorange"/>
                <stop offset="30%" stop-opacity="0" stop-color="darkorange"/>
                <stop offset="100%" stop-opacity="0" stop-color="darkorange"/>
            </linearGradient>
            <circle cx="50" cy="50" r="45" fill="url(#lg1)" stroke="grey" stroke-width="5"/>
            <text x="53" y="45" font-family="Roboto" font-size="1em" fill="#000" text-anchor="middle" dy="45">30%</text>
            <text x="50" y="0" font-family="Roboto" font-size="1em" fill="#000" text-anchor="middle" dy="45">PHP</text>
          </svg>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" width="200" height="200">
            <linearGradient id="lg2" x1="0.5" y1="1" x2="0.5" y2="0">
                <stop offset="0%" stop-opacity="1" stop-color="royalblue"/>
                <stop offset="50%" stop-opacity="1" stop-color="royalblue"/>
                <stop offset="50%" stop-opacity="0" stop-color="royalblue"/>
                <stop offset="100%" stop-opacity="0" stop-color="royalblue"/>
            </linearGradient>
            <circle cx="50" cy="50" r="45" fill="url(#lg2)" stroke="grey" stroke-width="5"/>
            <text x="53" y="45" font-family="Roboto" font-size="1em" fill="#000" text-anchor="middle" dy="45">50%</text>
            <text x="50" y="0" font-family="Roboto" font-size="1em" fill="#000" text-anchor="middle" dy="45">Javascript</text>
          </svg>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" width="200" height="200">
            <linearGradient id="lg3" x1="0.5" y1="1" x2="0.5" y2="0">
                <stop offset="0%" stop-opacity="1" stop-color="forestgreen"/>
                <stop offset="80%" stop-opacity="1" stop-color="forestgreen"/>
                <stop offset="80%" stop-opacity="0" stop-color="forestgreen"/>
                <stop offset="100%" stop-opacity="0" stop-color="forestgreen"/>
            </linearGradient>
            <circle cx="50" cy="50" r="45" fill="url(#lg3)" stroke="grey" stroke-width="5"/>
            <text x="53" y="45" font-family="Roboto" font-size="1em" fill="#000" text-anchor="middle" dy="45">80%</text>
            <text x="50" y="0" font-family="Roboto" font-size="1em" fill="#000" text-anchor="middle" dy="45">jQuery</text>
          </svg>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" width="200" height="200">
            <linearGradient id="lg1" x1="0.5" y1="1" x2="0.5" y2="0">
                <stop offset="0%" stop-opacity="1" stop-color="darkorange"/>
                <stop offset="30%" stop-opacity="1" stop-color="darkorange"/>
                <stop offset="30%" stop-opacity="0" stop-color="darkorange"/>
                <stop offset="100%" stop-opacity="0" stop-color="darkorange"/>
            </linearGradient>
            <circle cx="50" cy="50" r="45" fill="url(#lg1)" stroke="grey" stroke-width="5"/>
            <text x="53" y="45" font-family="Roboto" font-size="1em" fill="#000" text-anchor="middle" dy="45">30%</text>
            <text x="50" y="0" font-family="Roboto" font-size="1em" fill="#000" text-anchor="middle" dy="45">PHP</text>
          </svg>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" width="200" height="200">
            <linearGradient id="lg2" x1="0.5" y1="1" x2="0.5" y2="0">
                <stop offset="0%" stop-opacity="1" stop-color="royalblue"/>
                <stop offset="50%" stop-opacity="1" stop-color="royalblue"/>
                <stop offset="50%" stop-opacity="0" stop-color="royalblue"/>
                <stop offset="100%" stop-opacity="0" stop-color="royalblue"/>
            </linearGradient>
            <circle cx="50" cy="50" r="45" fill="url(#lg2)" stroke="grey" stroke-width="5"/>
            <text x="53" y="45" font-family="Roboto" font-size="1em" fill="#000" text-anchor="middle" dy="45">50%</text>
            <text x="50" y="0" font-family="Roboto" font-size="1em" fill="#000" text-anchor="middle" dy="45">Javascript</text>
          </svg>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" width="200" height="200">
            <linearGradient id="lg3" x1="0.5" y1="1" x2="0.5" y2="0">
                <stop offset="0%" stop-opacity="1" stop-color="forestgreen"/>
                <stop offset="80%" stop-opacity="1" stop-color="forestgreen"/>
                <stop offset="80%" stop-opacity="0" stop-color="forestgreen"/>
                <stop offset="100%" stop-opacity="0" stop-color="forestgreen"/>
            </linearGradient>
            <circle cx="50" cy="50" r="45" fill="url(#lg3)" stroke="grey" stroke-width="5"/>
            <text x="53" y="45" font-family="Roboto" font-size="1em" fill="#000" text-anchor="middle" dy="45">80%</text>
            <text x="50" y="0" font-family="Roboto" font-size="1em" fill="#000" text-anchor="middle" dy="45">jQuery</text>
          </svg>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" width="200" height="200">
            <linearGradient id="lg1" x1="0.5" y1="1" x2="0.5" y2="0">
                <stop offset="0%" stop-opacity="1" stop-color="darkorange"/>
                <stop offset="30%" stop-opacity="1" stop-color="darkorange"/>
                <stop offset="30%" stop-opacity="0" stop-color="darkorange"/>
                <stop offset="100%" stop-opacity="0" stop-color="darkorange"/>
            </linearGradient>
            <circle cx="50" cy="50" r="45" fill="url(#lg1)" stroke="grey" stroke-width="5"/>
            <text x="53" y="45" font-family="Roboto" font-size="1em" fill="#000" text-anchor="middle" dy="45">30%</text>
            <text x="50" y="0" font-family="Roboto" font-size="1em" fill="#000" text-anchor="middle" dy="45">PHP</text>
          </svg>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" width="200" height="200">
            <linearGradient id="lg2" x1="0.5" y1="1" x2="0.5" y2="0">
                <stop offset="0%" stop-opacity="1" stop-color="royalblue"/>
                <stop offset="50%" stop-opacity="1" stop-color="royalblue"/>
                <stop offset="50%" stop-opacity="0" stop-color="royalblue"/>
                <stop offset="100%" stop-opacity="0" stop-color="royalblue"/>
            </linearGradient>
            <circle cx="50" cy="50" r="45" fill="url(#lg2)" stroke="grey" stroke-width="5"/>
            <text x="53" y="45" font-family="Roboto" font-size="1em" fill="#000" text-anchor="middle" dy="45">50%</text>
            <text x="50" y="0" font-family="Roboto" font-size="1em" fill="#000" text-anchor="middle" dy="45">Javascript</text>
          </svg>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" width="200" height="200">
            <linearGradient id="lg3" x1="0.5" y1="1" x2="0.5" y2="0">
                <stop offset="0%" stop-opacity="1" stop-color="forestgreen"/>
                <stop offset="80%" stop-opacity="1" stop-color="forestgreen"/>
                <stop offset="80%" stop-opacity="0" stop-color="forestgreen"/>
                <stop offset="100%" stop-opacity="0" stop-color="forestgreen"/>
            </linearGradient>
            <circle cx="50" cy="50" r="45" fill="url(#lg3)" stroke="grey" stroke-width="5"/>
            <text x="53" y="45" font-family="Roboto" font-size="1em" fill="#000" text-anchor="middle" dy="45">80%</text>
            <text x="50" y="0" font-family="Roboto" font-size="1em" fill="#000" text-anchor="middle" dy="45">jQuery</text>
          </svg>
        </div>
  </div>
</div>
@endsection

@section('fiveBlock')
<div class="mdl-grid">
  <div class="mdl-cell mdl-cell--12-col top-spacing-30">
    <div class="title-about">
      Contactar
    </div>
  </div>
  <div class="mdl-cell mdl-cell--4-col mdl-cell--1-offset top-spacing-60">
    <div class="demo-card-wide mdl-card mdl-shadow--2dp">
      <div class="mdl-card__title">
        <h2 class="mdl-card__title-text">A traves de:</h2>
      </div>
      <div class="mdl-card__supporting-text">
        <div class="mdl-chip mdl-cell--12-col">
          <span class="mdl-chip__text">
            <span class="mdl-chip__contact mdl-color-text--black material-icons">email</span>
            {{ $d['Email'] }}
          </span>
        </div>
        <div class="mdl-chip  mdl-cell--12-col">
          <span class="mdl-chip__text">
            <span class="mdl-chip__contact mdl-color-text--black mat-icon material-icons">phone</span>
            {{ $d['Fijo'] }}
          </span>
        </div>
        <div class="mdl-chip  mdl-cell--12-col">
          <span class="mdl-chip__text">
            <span class="mdl-chip__contact mdl-color-text--black mat-icon material-icons">phone_iphone</span>
            {{ $d['Movil'] }}
          </span>
        </div>
        <div class="mdl-chip  mdl-cell--12-col">
          <span class="mdl-chip__text">
            <span class="mdl-chip__contact mdl-color-text--black mat-icon material-icons">person_pin_circle</span>
            {{  $d['Calle'] . ' ' . $d['Numero']  . ' - ' . $d['cPostal'] }}
          </span>
        </div>
        <div class="mdl-chip  mdl-cell--12-col">
          <span class="mdl-chip__text">
            <span class="mdl-chip__text"><dd>{{ $d['Poblacion'] . ' | ' . $d['Pais'] }}</dd></span>
          </span>
        </div>
        <div class="mdl-chip  mdl-cell--12-col">
          <span class="mdl-chip__text">
            <span class="mdl-chip__contact mdl-color-text--black mat-icon material-icons">file_download</span>
            <span class="mdl-chip__text"><a href="{{ asset('docs/CV - RGC.pdf')}}" style="color: #07dbff">PDF</a>  -  <a href="{{ asset('docs/CV - RGC.docx')}}" style="color: #07dbff">DOC</a></span>
          </span>
        </div>
      </div>      
    </div>
  </div>
  <div class="mdl-cell mdl-cell--6-col top-spacing-60">
    <div class="demo-card-wide mdl-card mdl-shadow--2dp mdl-cell--12-col">
      <div class="mdl-card__title">
        <h2 class="mdl-card__title-text">Escribeme:</h2>
      </div>
      <form action="{{ url('/') }}" method="POST">
       <div class="mdl-card__supporting-text">
          {{ csrf_field() }}
          <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
              {{ Form::text('nombre', null, ['class' => 'mdl-textfield__input']) }}
              {{ Form::label('nombre', 'Nombre de contacto', ['class' => 'mdl-textfield__label']) }}
          </div>
          <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
              {{ Form::text('email', null, ['class' => 'mdl-textfield__input']) }}
              {{ Form::label('email', 'E-mail de contacto', ['class' => 'mdl-textfield__label']) }}
          </div>
          <div class="mdl-textfield mdl-js-textfield mdl-cell--12-col">
              {{ Form::textarea('mensaje', null, ['id' => 'text-editor']) }}
          </div>
        </div>
        <div class="mdl-card__actions mdl-card--border">
            {{ Form::submit('Enviar mensaje', ['class' => 'mdl-button mdl-js-button mdl-js-ripple-effect']) }}
        </div>
      </form>
    </div>
  </div>
</div>
@endsection