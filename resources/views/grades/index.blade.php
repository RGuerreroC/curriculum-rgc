@extends('back')

@section('content')
<div class="mdl-cell mdl-cell--6-col mdl-cell--12-phone mdl-cell--3-offset-desktop top-spacing-50">
  <div class="mdl-card mdl-shadow--0dp mdl-cell--12-col mdl-cell--10-phone">
    <div class="mdl-card__title">
      <h2 class="mdl-card__title-text">Estudios</h2>
    </div>
    <div class="mdl-card__supporting-text mdl-cell--12-phone">
      @foreach($grades as $i => $grade)
        @if($i != 0)
          <hr>
        @endif
        <div class="mdl-card mdl-shadow--0dp mdl-cell--12-col mdl-cell--10-phone">
          <div class="mdl-card__title">
            <h2 class="mdl-card__title-text">{{ $grade->colegio }}</h2>
          </div>
          <div class="mdl-card__supporting-text mdl-cell--12-phone">
            <p>{{ $grade->titulo }}</p>
          </div>
          <div class="mdl-card__actions">
            <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect modal__trigger" data-modal="#modal{{$grade->id}}">+ info</a>
            <a href="{{ route('grades.edit', $grade->id) }}" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">Editar</a>
          </div>
        </div>
      @endforeach
    </div>
    <div class="mdl-card__menu">
      <button class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect modal__trigger" data-modal="#modalNewGrade">
        <i class="material-icons">add</i>
      </button>
    </div>
  </div>
  <!-- Modal Structure -->
  <div id="modalNewGrade" class="modal modal__bg">
    <div class="modal__dialog">
      @include('grades/create')
    </div>
  </div>
</div>
@foreach($grades as $i => $grade)
<!-- Modal Structure -->
<div id="modal{{$grade->id}}" class="modal modal__bg">
  <div class="modal__dialog">
    <div class="modal__content">
      <div class="modal__header">
        <div class="modal__title">
          <h2 class="modal__title-text">{{ $grade->colegio }}</h2>
        </div>
        <span class="mdl-button mdl-button--icon mdl-js-button material-icons modal__close"></span>
      </div>
      <div class="modal__text">
         {!! $grade->descripcion !!}
      </div>
      <div class="modal__footer">
        <a class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored modal__close">Cerrar</a>
      </div>
    </div>
  </div>
</div>
@endforeach
@endsection
