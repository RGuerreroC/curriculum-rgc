@extends('back')

@section('content')
<div class="mdl-cell mdl-cell--6-col mdl-cell--12-phone mdl-cell--3-offset-desktop top-spacing-50">
  <div class="mdl-card mdl-shadow--3dp mdl-cell--12-col mdl-cell--10-phone">
    <div class="mdl-card__title">
      <h2 class="mdl-card__title-text">Estudio</h2>
    </div>
    <div class="mdl-card__supporting-text mdl-cell--12-phone">
    {{ Form::model($grade,['route' => ['grades.update', $grade->id], 'files' => 'false', 'method' => 'PUT']) }}
      <div class="mdl-cell--5-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        {{ Form::text('titulo',null, ['class' => 'mdl-textfield__input']) }}
        {{ Form::label('titulo','Titulo', ['class' => 'mdl-textfield__label']) }}
      </div>
      <div class="mdl-cell--5-col mdl-cell--1-offset mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
          {{ Form::text('colegio',null, ['class' => 'mdl-textfield__input']) }}
          {{ Form::label('colegio','Colegio', ['class' => 'mdl-textfield__label']) }}
      </div>
      <div class="mdl-cell--5-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        {{ Form::date('fecha_inicio',null, ['class' => 'mdl-textfield__input']) }}
        {{ Form::label('fecha_inicio','Fecha Inicio', ['class' => 'mdl-textfield__label']) }}
      </div>
      <div class="mdl-cell--5-col mdl-cell--1-offset mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        {{ Form::date('fecha_fin',null, ['class' => 'mdl-textfield__input']) }}
        {{ Form::label('fecha_fin','Fecha fin', ['class' => 'mdl-textfield__label']) }}
      </div>
      <div class="mdl-cell--12-col">
        {{ Form::textarea('descripcion',null, ['id' => 'text-editor']) }}
      </div>
    <div class="card-footer">
      <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">Actualizar</button>
    </div>
    {{ Form::close() }}
    </div>
    <div class="mdl-card__menu">
      <button class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect modal__trigger" data-modal="#modalDelEstudio">
        <i class="material-icons">delete</i>
      </button>
    </div>
  </div>
</div>
<!-- Modal Structure -->
<div id="modalDelEstudio" class="modal modal__bg">
  <div class="modal__dialog">
    <div class="modal__content">
      {!! Form::open(['route' => ['grades.destroy', $grade->id], 'method' => 'DELETE']) !!}
      <div class="modal__header">
        <div class="modal__title">
          <h2 class="modal__title-text">Borrar Estudio</h2>
        </div>
        <span class="mdl-button mdl-button--icon mdl-js-button material-icons modal__close"></span>
      </div>
      <div class="modal__text">
        <p>
          Deseas eliminar el estudio?
        </p>
        <p>
          <sub>Se eliminará para siempre y no se podra recuperar</sub>
        </p>
      </div>
      <div class="modal__footer">
        <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">Borrar</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
@endsection