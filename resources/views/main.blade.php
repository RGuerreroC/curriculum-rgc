<!doctype html>
<html lang="{{ app()->getLocale() }}">
    @include('_statics._header')
    <body>
        @include('_statics._messages')
        <div id="fixedBlock">
          @yield('fixedBlock')
        </div>
        <div id="inicioBlock" class="inicioBlock odd">
            @yield('oneBlock')
        </div>
        <div id="sobreMiBlock" class="sobreMiBlock">
            @yield('twoBlock')
        </div>
        <div id="experienciaBlock" class="experienciaBlock odd">
            @yield('threeBlock')
        </div>
        <div id="habilidadesBlock" class="habilidadesBlock">
            @yield('fourBlock')
        </div>
        <div id="contactoBlock" class="contactoBlock odd">
            @yield('fiveBlock')
        </div>
        @include('_statics._globalJS')

        @yield('scripts')
    </body>
</html>
