<!doctype html>
<html lang="{{ app()->getLocale() }}">
    @include('_statics._header')

    <body>
        @include('_statics._nav')
        <main class="mdl-layout__content">
          @include('_statics._messages')
          <div class="mdl-grid">
            @yield('content')
          </div>
        </main>
      </div>
        @include('_statics._globalJS')
        
        @yield('scripts')
    </body>
</html>
