@if(Session::has('success'))
<dialog class="mdl-dialog  mdl-color--teal top-spacing-50">
  <h4 class="mdl-dialog__title"></h4>
  <div class="mdl-dialog__content mdl-color-text--white">
    <p>
      {{ Session::get('success') }}
    </p>
  </div>
  <div class="mdl-dialog__actions">
    <button type="button" class="mdl-button close">Cerrar</button>
  </div>
</dialog>
@elseif(Session::has('error'))
<dialog class="mdl-dialog  mdl-color--red top-spacing-50">
  <h4 class="mdl-dialog__title"></h4>
  <div class="mdl-dialog__content mdl-color-text--white">
    <p>
      {{ Session::get('error') }}
    </p>
  </div>
  <div class="mdl-dialog__actions">
    <button type="button" class="mdl-button close">Cerrar</button>
  </div>
</dialog>
@endif

@if(count($errors) > 0)
<dialog class="mdl-dialog  mdl-color--red top-spacing-50">
  <h4 class="mdl-dialog__title">Errores:</h4>
  <div class="mdl-dialog__content mdl-color-text--white">
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  <div class="mdl-dialog__actions">
    <button type="button" class="mdl-button close">Cerrar</button>
  </div>
</dialog>
@endif

@section('scripts')
<script>
  var dialog = document.querySelector('dialog');
  
  if(dialog != null){
    if (! dialog.showModal) {
      dialogPolyfill.registerDialog(dialog);
    }

    dialog.showModal();

    dialog.querySelector('.close').addEventListener('click', function() {
      dialog.close();
    });
  }
</script>
@endsection
