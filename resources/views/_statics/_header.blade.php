<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <title>Raul Guerrero | CV @yield('title')</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">

    <!--link rel="stylesheet" href="http://cdn.dxcodercrew.net/materialize/dist/css/materialize.min.css"-->
    <link rel="stylesheet" href="http://cdn.dxcodercrew.net/material-design-lite/material.min.css" />
    <link rel="stylesheet" href="http://cdn.dxcodercrew.net/material-design-lite/material-teal-green.min.css" />
    <link rel="stylesheet" href="http://cdn.dxcodercrew.net/material-modal/dist/css/material-modal.min.css" />
    <link rel="stylesheet" href="http://cdn.dxcodercrew.net/trumbowyg/dist/ui/trumbowyg.min.css">
    <link rel='stylesheet' href='http://cdn.dxcodercrew.net/font-awesome/4.7.0/css/font-awesome.css' />
    <link rel='stylesheet' href='http://cdn.dxcodercrew.net/fontello/css/fontello.css' />
    <!-- Estilos propios -->
    {{ Html::style('css/styles.css') }}
    {{ Html::style('css/social-icons.css') }}
    {{ Html::style('css/mdl-custom.css') }}
    @yield('stylesheets')
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
