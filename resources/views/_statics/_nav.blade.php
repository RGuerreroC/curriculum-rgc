<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
  <header class="mdl-layout__header">
    <div class="mdl-layout__header-row">
      <span class="mdl-layout-title">
        CV | Raul Guerrero
      </span>
      <div class="mdl-layout-spacer"></div>
      @if(Auth::check())
        <button id="demo-menu-lower-right"
                class="mdl-button mdl-js-button mdl-button--icon">
          <i class="material-icons">more_vert</i>
        </button>
        <nav class="mdl-navigation">
          <a href="{{ route('auth.logoff') }}" class="mdl-navigation__link">
            <i class="material-icons">power_settings_new</i>
          </a>
        </nav>
        <ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
            for="demo-menu-lower-right">
          <li class="mdl-menu__item"><a href="{{ route('datos.index') }}" class="mdl-navigation__link mdl-button--colored">Datos</a></li>
          <li class="mdl-menu__item"><a href="{{ route('social.index') }}" class="mdl-navigation__link">Social</a></li>
          <li class="mdl-menu__item"><a href="{{ route('hobbies.index') }}" class="mdl-navigation__link">Hobbies</a></li>
          <li class="mdl-menu__item"><a href="{{ route('jobs.index') }}" class="mdl-navigation__link">Trabajos</a></li>
          <li class="mdl-menu__item"><a href="{{ route('grades.index') }}" class="mdl-navigation__link">Estudios</a></li>
          <li class="mdl-menu__item"><a href="{{ route('skills.index') }}" class="mdl-navigation__link">Habilidades</a></li>
        </ul>
      @else
        <nav class="mdl-navigation">
          <a href="{{ route('auth.login') }}" class="mdl-navigation__link">Acceder</a>
        </nav>
      @endif
    </div>
  </header>
