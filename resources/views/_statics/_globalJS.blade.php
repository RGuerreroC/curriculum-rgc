<!-- Scripts -->
<script type="text/javascript" charset="utf-8" src="http://cdn.dxcodercrew.net/jQuery/1.12.3/jquery.min.js"> </script>
<script type="text/javascript" charset="utf-8" src="http://cdn.dxcodercrew.net/material-design-lite/material.min.js"></script>
<script type="text/javascript" charset="utf-8" src="http://cdn.dxcodercrew.net/material-modal/dist/js/material-modal.min.js"></script>
<script type="text/javascript" charset="utf-8" src="http://cdn.dxcodercrew.net/trumbowyg/dist/trumbowyg.min.js"></script>
<script type="text/javascript" charset="utf-8" src="http://cdn.dxcodercrew.net/trumbowyg/dist/langs/es.min.js"></script>
<script type="text/javascript">

$(function(){
  /**
   * Configuracion del editor WYSIWYG
   */
  var textConfig = {
    lang: 'es',
    btns: [
        ['viewHTML'],
        ['undo', 'redo'], // Only supported in Blink browsers
        ['formatting'],
        ['strong', 'em', 'del'],
        ['superscript', 'subscript'],
        ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
        ['unorderedList', 'orderedList'],
        ['removeformat']
    ],
    autogrow: true
  };
  
  $("#text-editor").trumbowyg(textConfig);
});
</script>
