@extends('back')

@section('content')

    <div class="row">
        <div class="col s12 m4 l4 offset-m4 offset-l4">
            <div class="card">
                <div class="card-content">
                    <div class="card-title">Registro</div>
                    <div class="panel-body">
                        {{ Form::open() }}
                        {{ csrf_field() }}
                        <div class="input-field">
                            {{ Form::text('name', null) }}
                            {{ Form::label('name', 'Nombre') }}
                        </div>
                        <div class="input-field">
                            {{ Form::text('email', null) }}
                            {{ Form::label('email', 'e-Mail') }}
                        </div>
                        <div class="input-field">
                            {{ Form::password('password', null) }}
                            {{ Form::label('password', 'Contraseña') }}
                        </div>
                        <div class="input-field">
                            {{ Form::password('password_confirmation', null) }}
                            {{ Form::label('password_confirmation', 'Verificar contraseña') }}
                        </div>
                        <div class="center">
                            {{ Form::submit('Registar', ['class' => 'btn waves-effect waves-red green']) }}
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
