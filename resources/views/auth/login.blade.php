@extends('back')

@section('content')
<div class="mdl-cell mdl-cell--12-col">
  <div class="mdl-cell--4-col mdl-cell--4-offset">
    {{ Form::open() }}
      {{ csrf_field() }}
      <div class="mdl-cell--12-col mdl-card mdl-shadow--2dp">
        <div class="mdl-card__title">
          <h2 class="mdl-card__title-text">Login</h2>
        </div>
        <div class="mdl-card__supporting-text">
          <div class="mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
            {{ Form::text('email', null, ['class' => 'mdl-textfield__input']) }}
            {{ Form::label('email', 'e-Mail', ['class' => 'mdl-textfield__label']) }}
          </div>
          <div class="mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
            {{ Form::password('password', ['class' => 'mdl-textfield__input']) }}
            {{ Form::label('password', 'Contraseña', ['class' => 'mdl-textfield__label']) }}
          </div>
        </div>
      <div class="mdl-card__actions mdl-card--border">
        {{ Form::submit('Login', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-color--green']) }}
      </div>
    </div>
    {{ Form::close() }}
  </div>
</div>
@endsection
