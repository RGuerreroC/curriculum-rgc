{!! Form::open(['route' => 'datos.store']) !!}
<div class="modal__content">
  <div class="modal__header">
    <div class="modal__title">
      <h2 class="modal__title-text">Nuevo Dato</h2>
    </div>
    <span class="mdl-button mdl-button--icon mdl-js-button material-icons modal__close"></span>
  </div>
  <div class="modal__text">
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
      {{ Form::text('nombre',null, ['class' => 'mdl-textfield__input']) }}
      {{ Form::label('nombre','Nombre', ['class' => 'mdl-textfield__label']) }}
    </div>
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
      {{ Form::text('dato',null, ['class' => 'mdl-textfield__input']) }}
      {{ Form::label('dato','Dato', ['class' => 'mdl-textfield__label']) }}
    </div>
  </div>
  <div class="modal__footer">
    <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">Guardar</button>
  </div>
</div>
{!! Form::close() !!}