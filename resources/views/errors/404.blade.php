@extends('error')


@section('content')
<style>
  body{
    background-color: lightblue;
  }
  .mdl-card{
    background-color: transparent;
  }
</style>
<div class="mdl-cell mdl-cell--12-col mdl-card mdl-shadow--0dp">
  <div class="mdl-card__title">
    <h2 class="mdl-card__title-text">404</h2>
  </div>
  <div class="mdl-card__supporting-text">
    Ups.... La página no existe
  </div>
  <div class="mdl-card__actions mdl-card--border">
    <!-- Colored FAB button with ripple -->
    <a href="datos"class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-color--white  mdl-cell--5-offset">
      <i class="material-icons  mdl-color--white-text">home</i>
    </a>
  </div>
</div>
@endsection