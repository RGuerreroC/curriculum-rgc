@extends('back')

@section('content')
<div class="mdl-cell mdl-cell--6-col mdl-cell--12-phone mdl-cell--3-offset-desktop top-spacing-50">
  <div class="mdl-card mdl-shadow--0dp mdl-cell--12-col mdl-cell--10-phone">
    <div class="mdl-card__title">
      <h2 class="mdl-card__title-text">Habilidades</h2>
    </div>
    <div class="mdl-card__supporting-text mdl-cell--12-phone">
      <table class="mdl-data-table mdl-js-data-table mdl-cell--12-col">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Porcentaje</th>
                <th>Editar</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($skills as $i => $skill)
                <tr>
                    <td style="text-transform: capitalize">{{ $skill->nombre }}</td>
                    <td>
                      {{ $skill->porcentaje }}%
                    </td>
                    <td><a href="{{ route('social.edit', $rs->id) }}" ><i class="fa fa-pencil fa-lg mdl-color-text--black"></i></a></td>
                </tr>
            @endforeach
        </tbody>
      </table>
    </div>
    <div class="mdl-card__menu">
      <button class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect modal__trigger" data-modal="#modalNewDato">
        <i class="material-icons">add</i>
      </button>
    </div>
  </div>
  <!-- Modal Structure -->
  <div id="modalNewDato" class="modal modal__bg">
    <div class="modal__dialog">
      @include('skills/create')
    </div>
  </div>
</div>
@endsection