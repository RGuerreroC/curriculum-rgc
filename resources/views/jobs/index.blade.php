@extends('back')

@section('content')
<div class="mdl-cell mdl-cell--6-col mdl-cell--12-phone mdl-cell--3-offset-desktop top-spacing-50">
  <div class="mdl-card mdl-shadow--0dp mdl-cell--12-col mdl-cell--10-phone">
    <div class="mdl-card__title">
      <h2 class="mdl-card__title-text">Trabajos</h2>
    </div>
    <div class="mdl-card__supporting-text mdl-cell--12-phone">
      @foreach($jobs as $i => $job)
        @if($i != 0)
          <hr>
        @endif
        <div class="mdl-card mdl-shadow--0dp mdl-cell--12-col mdl-cell--10-phone">
          <div class="mdl-card__title">
            <h2 class="mdl-card__title-text">{{ $job->empresa }}</h2>
          </div>
          <div class="mdl-card__supporting-text mdl-cell--12-phone">
            <p>{{ $job->titulo }}</p>
          </div>
          <div class="mdl-card__actions">
            <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect modal__trigger" data-modal="#modal{{$job->id}}">+ info</a>
            <a href="{{ route('jobs.edit', $job->id) }}" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">Editar</a>
          </div>
        </div>
      @endforeach
    </div>
    <div class="mdl-card__menu">
      <button class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect modal__trigger" id="addData" data-modal="#modalNewDato">
        <i class="material-icons">add</i>
      </button>
      <div class="mdl-tooltip" data-mdl-for="addData">
          Añadir nuevo dato
        </div>
    </div>
  </div>
  <!-- Modal Structure -->
  <div id="modalNewDato" class="modal modal__bg">
    <div class="modal__dialog">
      @include('jobs/create')
    </div>
  </div>
</div>
@foreach($jobs as $i => $job)
<!-- Modal Structure -->
<div id="modal{{$job->id}}" class="modal modal__bg">
  <div class="modal__dialog">
    <div class="modal__content">
      <div class="modal__header">
        <div class="modal__title">
          <h2 class="modal__title-text">{{ $job->empresa }}</h2>
        </div>
        <span class="mdl-button mdl-button--icon mdl-js-button material-icons modal__close"></span>
      </div>
      <div class="modal__text">
         {!! $job->descripcion !!}
      </div>
      <div class="modal__footer">
        <a class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored modal__close">Cerrar</a>
      </div>
    </div>
  </div>
</div>
@endforeach
@endsection