{!! Form::open(['route' => 'jobs.store']) !!}
<div class="modal__content">
  <div class="modal__header">
    <div class="modal__title">
      <h2 class="modal__title-text">Nuevo Trabajo</h2>
    </div>
    <span class="mdl-button mdl-button--icon mdl-js-button material-icons modal__close"></span>
  </div>
  <div class="modal__text mdl-cell mdl-cell--12-col">
    <div class="mdl-cell--5-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
      {{ Form::text('titulo',null, ['class' => 'mdl-textfield__input']) }}
      {{ Form::label('titulo','Ocupacion', ['class' => 'mdl-textfield__label']) }}
    </div>
    <div class="mdl-cell--5-col mdl-cell--1-offset mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        {{ Form::text('empresa',null, ['class' => 'mdl-textfield__input']) }}
        {{ Form::label('empresa','Empresa', ['class' => 'mdl-textfield__label']) }}
    </div>
    <div class="mdl-cell--5-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
      {{ Form::date('fecha_inicio',null, ['class' => 'mdl-textfield__input']) }}
      {{ Form::label('fecha_inicio','Fecha Inicio', ['class' => 'mdl-textfield__label']) }}
    </div>
    <div class="mdl-cell--5-col mdl-cell--1-offset mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
      {{ Form::date('fecha_fin',null, ['class' => 'mdl-textfield__input']) }}
      {{ Form::label('fecha_fin','Fecha fin', ['class' => 'mdl-textfield__label']) }}
    </div>
    <div class="mdl-cell--12-col">
      {{ Form::textarea('descripcion',null, ['id' => 'text-editor']) }}
    </div>
  </div>
  <div class="modal__footer">
    <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">Guardar</button>
  </div>
</div>
{!! Form::close() !!}