<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middlewareGroups' => ['web']], function(){
    // Authentication Routes
    Route::get('login', ['as' => 'auth.login', 'uses' => 'Auth\LoginController@showLoginForm']);
    Route::post('login', ['as' => 'auth.login', 'uses' => 'Auth\LoginController@login']);
    Route::get('logoff', ['as' => 'auth.logoff', 'uses' => 'Auth\LoginController@logout']);

    // Register Routes
    //Route::get('auth/register', ['as' => 'auth.register', 'uses' => 'Auth\RegisterController@showRegistrationForm']);
    //Route::post('auth/register', ['as' => 'auth.register', 'uses' => 'Auth\RegisterController@register']);


    // Social Resources
    Route::resource('social', 'SocialController',['except' => ['create', 'show']]);
    // Datos Resources
    Route::resource('datos', 'DatosController',['except' => ['create', 'show']]);
    // Hobbies Resources
    Route::resource('hobbies', 'HobbiesController',['except' => ['create', 'show']]);
    // Jobs Resources
    Route::resource('jobs', 'JobsController',['except' => ['create', 'show']]);
    // Jobs Resources
    Route::resource('grades', 'GradesController',['except' => ['create', 'show']]);
    // Skills Resources
    Route::resource('skills', 'SkillsController',['except' => ['create', 'show']]);
  
    // Inici routes
    Route::get('/', ['as' => 'inicio', 'uses' => 'PagesController@getIndex']);
    Route::post('/', ['as' => 'inicio', 'uses' => 'PagesController@sendContact']);
});
