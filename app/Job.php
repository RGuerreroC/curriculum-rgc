<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    /**
     * Configuracion para eliminar el autoincremental por el uso de UUIDs
     */
    protected $primaryKey = "id";
    public $incrementing = false; 
}
