<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use App\Hobby;
use Session;

class HobbiesController extends Controller
{
    /**
     * Permite el acceso a los usuarios que pertenezcan al middleware auth
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hobs = Hobby::orderBy('created_at', 'asc')->get();
        return view('hobbies.index')->withHobs($hobs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Not in use
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
             'nombre' => 'required|max:255'
            ,'fa_icon' => 'required|max:255'
        ]);

        $hob = new Hobby;

        $hob->ID = Uuid::generate();
        $hob->nombre = $request->nombre;
        $hob->fa_icon = $request->fa_icon;

        $hob->save();

        Session::flash('success', 'Hobby guardado');

        return redirect()->route('hobbies.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $hob = Hobby::find($id);
        return view('hobbies/edit')
          ->withHob($hob);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
             'nombre' => 'required|max:255'
            ,'fa_icon' => 'required|max:255'
        ]);

        $hob = Hobby::find($id);

        $hob->nombre = $request->nombre;
        $hob->fa_icon = $request->fa_icon;

        $hob->save();

        Session::flash('success', 'Hobby guardado');

        return redirect()->route('hobbies.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hob = Hobby::find($id);
        if($hob->delete()){
          Session::flash('success', 'Dato eliminado correctamente');
        }else{
          Session::flash('error', 'Dato no eliminado');
        }

        return redirect()->route('hobbies.index');
    }
}
