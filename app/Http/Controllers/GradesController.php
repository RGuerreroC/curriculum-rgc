<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use App\Grade;
use Session;

class GradesController extends Controller
{
    /**
     * Permite el acceso a los usuarios que pertenezcan al middleware auth
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grades = Grade::orderBy('fecha_inicio', 'desc')->get();
        return view('grades.index')->withGrades($grades);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Not in use
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'titulo' => 'required',
          'colegio' => 'required',
          'fecha_inicio' => 'required'
        ]);
      
        $grade = new Grade;
        
        $grade->id = Uuid::generate();
        $grade->titulo = $request->titulo;
        $grade->colegio = $request->colegio;
        $grade->fecha_inicio = $request->fecha_inicio;
        $grade->fecha_fin = $request->fecha_fin;
        $grade->descripcion = $request->descripcion;
      
        $grade->save();
        Session::flash('success', 'Estudios guardado');

        return redirect()->route('grades.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Not in use
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $grade = Grade::find($id);
        return view('grades/edit')
          ->withGrade($grade);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'titulo' => 'required',
          'colegio' => 'required',
          'fecha_inicio' => 'required'
        ]);
      
        $grade = Grade::find($id);
        
        $grade->titulo = $request->titulo;
        $grade->colegio = $request->colegio;
        $grade->fecha_inicio = $request->fecha_inicio;
        $grade->fecha_fin = $request->fecha_fin;
        $grade->descripcion = $request->descripcion;
      
        $grade->save();
        Session::flash('success', 'Estudios guardado');

        return redirect()->route('grades.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $grade = Grade::find($id);
      if($grade->delete()){
        Session::flash('success', 'Dato eliminado correctamente');
      }else{
        Session::flash('error', 'Dato no eliminado');
      }
      return redirect()->route('grades.index');
    }
}
