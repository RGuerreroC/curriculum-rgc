<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Social;
use App\Dato;
use App\Hobby;
use App\Job;
use App\Grade;
use App\Mail\UserMailNotify;
use Illuminate\Support\Facades\Mail;
use Session;

class PagesController extends Controller
{
	/**
	 * Envia email de contacto desde el formulario
	 *
	 * @param Request $request
	 * @return void
	 */
	public function sendContact(Request $request){
        $this->validate($request, [
			'email' => 'required|email'
			,'nombre' => 'required'
			,'mensaje' => 'min:10'
		]);

		$data = array(
			'email' => $request->email
			,'nombre' => $request->nombre
			,'mensaje' => $request->mensaje
		);

		Mail::send('emails.usrContactMail', $data, function($message) use ($data){
			$message->from('dxcodercrew@gmail.com', 'Raul Guerrero :: DX Coder Crew')
					->to($data['email'])
					->subject('Mensaje recibido '.$data['nombre'].'!!');
		});

		Mail::send('emails.ownContactMail', $data, function($message) use ($data){
			$message->from('dxcodercrew@gmail.com', 'Raul Guerrero :: DX Coder Crew')
					->to('r.g.c@me.com')
					->subject('Mensaje de '.$data['nombre'].' recibido!!');
		});

        Session::flash('success', "El mensaje se ha enviado correctamente");

		return redirect()->action('PagesController@getIndex');

    }

	/**
	 * Recoge datos de la base de datos y los muestra en la vista publica
	 *
	 * @return void
	 */
	public function getIndex()
    {
        $datos = Dato::all();
        $rrss = Social::orderBy('created_at', 'asc')->get();
				$hobs = Hobby::orderBy('created_at', 'asc')->get();
        $jobs = Job::orderBy('fecha_inicio', 'desc')->get();
        $grades = Grade::orderBy('fecha_inicio', 'desc')->get();

        return view('pages.index')
          ->withDatos($datos)
          ->withRrss($rrss)
					->withHobs($hobs)
          ->withJobs($jobs)
          ->withGrades($grades);
    }
}
