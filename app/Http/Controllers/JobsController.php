<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use App\Job;
use Session;

class JobsController extends Controller
{
    /**
     * Permite el acceso a los usuarios que pertenezcan al middleware auth
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs = Job::orderBy('fecha_inicio', 'desc')->get();
        return view('jobs.index')->withJobs($jobs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Not in use
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'titulo' => 'required',
          'empresa' => 'required',
          'fecha_inicio' => 'required'
        ]);
      
        $job = new Job;
        
        $job->id = Uuid::generate();
        $job->titulo = $request->titulo;
        $job->empresa = $request->empresa;
        $job->fecha_inicio = $request->fecha_inicio;
        $job->fecha_fin = $request->fecha_fin;
        $job->descripcion = $request->descripcion;
      
        $job->save();
        Session::flash('success', 'Trabajo guardado');

        return redirect()->route('jobs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job = Job::find($id);
        return view('jobs/edit')
          ->withJob($job);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'titulo' => 'required',
          'empresa' => 'required',
          'fecha_inicio' => 'required'
        ]);
      
        $job = Job::find($id);
        
        $job->titulo = $request->titulo;
        $job->empresa = $request->empresa;
        $job->fecha_inicio = $request->fecha_inicio;
        $job->fecha_fin = $request->fecha_fin;
        $job->descripcion = $request->descripcion;
      
        $job->save();
        Session::flash('success', 'Trabajo guardado');

        return redirect()->route('jobs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $job = Job::find($id);
      if($job->delete()){
        Session::flash('success', 'Dato eliminado correctamente');
        }else{
          Session::flash('error', 'Dato no eliminado');
        }

        return redirect()->route('jobs.index');
    }
}
