<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use App\Social;
use Session;

class SocialController extends Controller
{
    /**
     * Permite el acceso a los usuarios que pertenezcan al middleware auth
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rrss = Social::orderBy('created_at', 'asc')->get();
        return view('rrss.index')
          ->withRrss($rrss);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
             'rrss' => 'required|max:255'
            ,'url' => 'required|max:255'
        ]);

        $rs = new Social;

        $rs->id = Uuid::generate();
        $rs->rrss = $request->rrss;
        $rs->url = $request->url;

        $rs->save();

        Session::flash('success', 'Red Social guardada');

        return redirect()->route('social.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rs = Social::find($id);
        return view('rrss/edit')
          ->withRs($rs);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
             'rrss' => 'required|max:255'
            ,'url' => 'required|max:255'
        ]);

        $rs = Social::find($id);
        
        $rs->rrss = $request->rrss;
        $rs->url = $request->url;

        $rs->save();

        Session::flash('success', 'Red Social guardada');

        return redirect()->route('social.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $rs = Social::find($id);
      if($rs->delete()){
        Session::flash('success', 'Red Social eliminada correctamente');
      }else{
        Session::flash('error', 'Red Social no eliminada');
      }
      
      return redirect()->route('social.index');
    }
}
