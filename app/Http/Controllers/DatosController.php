<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use App\Dato;
use Session;

class DatosController extends Controller
{
    /**
     * Permite el acceso a los usuarios que pertenezcan al middleware auth
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos = Dato::orderBy('created_at', 'asc')->get();
        return view('datos.index')->withDatos($datos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
             'nombre' => 'required|max:255'
            ,'dato' => 'required|max:255'
        ]);

        $dato = new Dato;

        $dato->ID = Uuid::generate();
        $dato->nombre = $request->nombre;
        $dato->dato = $request->dato;

        $dato->save();

        Session::flash('success', 'Dato guardado');

        return redirect()->route('datos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dato = Dato::find($id);
        return view('datos/edit')
          ->withDato($dato);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
             'nombre' => 'required|max:255'
            ,'dato' => 'required|max:255'
        ]);

        $dato = Dato::find($id);

        $dato->nombre = $request->nombre;
        $dato->dato = $request->dato;

        $dato->save();

        Session::flash('success', 'Dato guardado');

        return redirect()->route('datos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $dato = Dato::find($id);
      if($dato->delete()){
        Session::flash('success', 'Dato eliminado correctamente');
      }else{
        Session::flash('error', 'Dato no eliminado');
      }
      
      return redirect()->route('datos.index');
    }
}
